package toDoLista;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Zapis {
    public boolean zapis(String path, String toSave){
        try {
            FileWriter myWriter = new FileWriter(path);
            myWriter.write(toSave);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
            return true;
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            return false;
        }
    }
}
