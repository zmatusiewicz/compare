package toDoLista;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Plan {
    public static Akcja wczytaj(){
        Scanner sc =new Scanner(System.in);
        Akcja akcja = new Akcja();
        System.out.println("Podaj nazwa: ");
        akcja.setNazwa(sc.nextLine());
        if(akcja.getNazwa().equals("!")){
            return akcja;
        }
        System.out.println("Podaj opis: ");
        akcja.setOpisAkcji(sc.nextLine());
        System.out.println("Podaj termin: ");
        akcja.setTerminAkcji(sc.nextLine());
        System.out.println("Podaj pilność 1 -4 : ");
        int pilnosc = sc.nextInt();
        switch(pilnosc){
            case 1: akcja.setPriorytet(Priority.VERY_IMPORTANT);
            break;
            case 2: akcja.setPriorytet(Priority.IMPORTANT);
                break;
            case 3: akcja.setPriorytet(Priority.MIDLE);
                break;
            case 4: akcja.setPriorytet(Priority.VERY_IMPORTANT);
                break;
            default:{
                akcja.setPriorytet(Priority.NOT_IMPORTANT);
                break;
            }
        }

        return akcja;
    }
    public static void main(String[] args) {
        List<Akcja> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        Akcja rzecz;
        do {
            System.out.print("podaj aktywność: ");
            rzecz = wczytaj();
            if(!rzecz.getNazwa().equals("!")) {
                lista.add(rzecz);
            }
        }while(!rzecz.getNazwa().equals("!"));
        Collections.sort(lista, new CompareAkcja());
        Collections.sort(lista, (a,b)->{{
            int iPriorytet;

            // 7, 14, 23, etc.
            int iCzas;
            if(Integer.parseInt(a.getTerminAkcji()) > Integer.parseInt(b.getTerminAkcji())){
                return 1;
            }else if(Integer.parseInt(a.getTerminAkcji()) < Integer.parseInt(b.getTerminAkcji())){
                return -1;
            } else return 0;

        }});
        System.out.println(lista);
        Zapis co = new Zapis();
        co.zapis("toDoList.txt", lista.toString());

/*
        BufferedReader plik2 = null;
        try {
            plik2 = new BufferedReader(new FileReader("toDoList.txt"));
            System.out.println("\n\nOdczyt buforowany:\n");
            String l = plik2.readLine();
            while (l != null) {
                System.out.println(l);
                l = plik2.readLine();
            }
        }catch(FileNotFoundException e){
            System.out.println("Nie ma pliku");

        }
        catch(IOException e){
            System.out.println("Błąd wczytania");
        }
        finally {
            if (plik2 != null) {
                try {
                    plik2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
    }
}
