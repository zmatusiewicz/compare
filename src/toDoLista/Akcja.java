package toDoLista;

public class Akcja {
    private String nazwa;
    private String opisAkcji;
    private String terminAkcji;
    private Priority priorytet; //1 - 4 1-Very, 2 -IM, 3 -MID, 4-LOW

    public Akcja() {
        this.priorytet = Priority.NOT_IMPORTANT;
    }

    public Akcja(String nazwa) {
        this.nazwa = nazwa;
        this.priorytet = Priority.NOT_IMPORTANT;
    }

    public Akcja(String nazwa, String terminAkcji) {
        this.nazwa = nazwa;
        this.terminAkcji = terminAkcji;
        this.priorytet = Priority.NOT_IMPORTANT;
    }

    public Akcja(String nazwa, Priority priorytet) {
        this.nazwa = nazwa;
        this.priorytet = priorytet;
    }

    public Akcja(String nazwa, String terminAkcji, Priority priorytet) {
        this.nazwa = nazwa;
        this.terminAkcji = terminAkcji;
        this.priorytet = priorytet;
    }

    public Akcja(String nazwa, String opisAkcji, String terminAkcji, Priority priorytet) {
        this.nazwa = nazwa;
        this.opisAkcji = opisAkcji;
        this.terminAkcji = terminAkcji;
        this.priorytet = priorytet;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpisAkcji() {
        return opisAkcji;
    }

    public void setOpisAkcji(String opisAkcji) {
        this.opisAkcji = opisAkcji;
    }

    public String getTerminAkcji() {
        return terminAkcji;
    }

    public void setTerminAkcji(String terminAkcji) {
        this.terminAkcji = terminAkcji;
    }

    public Priority getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(Priority priorytet) {
        this.priorytet = priorytet;
    }

    @Override
    public String toString() {
        String opisAkcjiStr=""+nazwa+" ";
        if((terminAkcji!=null) &&(!terminAkcji.isEmpty())){
            opisAkcjiStr +=terminAkcji+" ";
        }else{
            opisAkcjiStr +="\t";
        }
        if(!priorytet.equals(Priority.NOT_IMPORTANT)){
            opisAkcjiStr +=priorytet+" ";
        }else{
            opisAkcjiStr +="\t";
        }
        if((opisAkcji!= null) && (!opisAkcji.isEmpty())){
            opisAkcjiStr += opisAkcji;
        }

        return opisAkcjiStr;
    }
}
